# from django.urls import path
# from todos.views import TodoList, todo_list_list, todo_list_detail, todo_list_create, edit_list, delete_list, todo_item_create


# urlpatterns = [
#     path("", todo_list_list, name="todo_list_list"),
#     path("<int:id>/", todo_list_detail, name="todo_list_detail"),
#     path("create/", todo_list_create, name="todo_list_create"),
#     path("<int:id>/edit/", edit_list, name="edit_list"),
#     path("<int:id>/delete/", delete_list, name="delete_list"),
#     path("items/create/<int:list_id>/", todo_item_create, name="todo_item_create"),
# ]

# from django.urls import path
# from todos.views import (
#     todo_list_list,
#     todo_list_detail,
#     todo_list_create,
#     edit_list,
#     delete_list,
#     todo_item_create,
# )


# urlpatterns = [
#     path("", todo_list_list, name="todo_list_list"),
#     path("<int:id>/", todo_list_detail, name="todo_list_detail"),
#     path("create/", todo_list_create, name="todo_list_create"),
#     path("<int:id>/edit/", edit_list, name="edit_list"),
#     path("<int:id>/delete/", delete_list, name="delete_list"),
#     path("items/create/<int:list_id>/", todo_item_create, name="todo_item_create"),
# ]


from django.urls import path
from todos.views import (
    todo_list_list,
    todo_list_detail,
    todo_list_create,
    edit_list,
    delete_list,
    todo_item_create,
    todo_item_update
)


urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:id>/edit/", edit_list, name="edit_list"),
    path("<int:id>/delete/", delete_list, name="delete_list"),
    path("items/create/<int:list_id>/", todo_item_create, name="todo_item_create"),
    path("items/<int:id>/edit/", todo_item_update, name="todo_item_update"),
]
